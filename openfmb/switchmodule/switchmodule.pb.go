// Code generated by protoc-gen-go. DO NOT EDIT.
// source: switchmodule/switchmodule.proto

package switchmodule

import (
	fmt "fmt"
	proto "github.com/golang/protobuf/proto"
	_ "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb"
	commonmodule "gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/openfmb/commonmodule"
	math "math"
)

// Reference imports to suppress errors if they are not otherwise used.
var _ = proto.Marshal
var _ = fmt.Errorf
var _ = math.Inf

// This is a compile-time assertion to ensure that this generated file
// is compatible with the proto package it is being compiled against.
// A compilation error at this line likely means your copy of the
// proto package needs to be updated.
const _ = proto.ProtoPackageIsVersion3 // please upgrade the proto package

// OpenFMB specialization for switch control:  LN: Circuit switch   Name: XSWI
type SwitchDiscreteControlXSWI struct {
	// UML inherited base object
	LogicalNodeForControl *commonmodule.LogicalNodeForControl `protobuf:"bytes,1,opt,name=logicalNodeForControl,proto3" json:"logicalNodeForControl,omitempty"`
	// MISSING DOCUMENTATION!!!
	Pos                  *commonmodule.ControlDPC `protobuf:"bytes,2,opt,name=Pos,proto3" json:"Pos,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                 `json:"-"`
	XXX_unrecognized     []byte                   `json:"-"`
	XXX_sizecache        int32                    `json:"-"`
}

func (m *SwitchDiscreteControlXSWI) Reset()         { *m = SwitchDiscreteControlXSWI{} }
func (m *SwitchDiscreteControlXSWI) String() string { return proto.CompactTextString(m) }
func (*SwitchDiscreteControlXSWI) ProtoMessage()    {}
func (*SwitchDiscreteControlXSWI) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{0}
}

func (m *SwitchDiscreteControlXSWI) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchDiscreteControlXSWI.Unmarshal(m, b)
}
func (m *SwitchDiscreteControlXSWI) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchDiscreteControlXSWI.Marshal(b, m, deterministic)
}
func (m *SwitchDiscreteControlXSWI) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchDiscreteControlXSWI.Merge(m, src)
}
func (m *SwitchDiscreteControlXSWI) XXX_Size() int {
	return xxx_messageInfo_SwitchDiscreteControlXSWI.Size(m)
}
func (m *SwitchDiscreteControlXSWI) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchDiscreteControlXSWI.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchDiscreteControlXSWI proto.InternalMessageInfo

func (m *SwitchDiscreteControlXSWI) GetLogicalNodeForControl() *commonmodule.LogicalNodeForControl {
	if m != nil {
		return m.LogicalNodeForControl
	}
	return nil
}

func (m *SwitchDiscreteControlXSWI) GetPos() *commonmodule.ControlDPC {
	if m != nil {
		return m.Pos
	}
	return nil
}

// Switch discrete control
type SwitchDiscreteControl struct {
	// UML inherited base object
	ControlValue *commonmodule.ControlValue `protobuf:"bytes,1,opt,name=controlValue,proto3" json:"controlValue,omitempty"`
	// MISSING DOCUMENTATION!!!
	Check *commonmodule.CheckConditions `protobuf:"bytes,2,opt,name=check,proto3" json:"check,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchDiscreteControlXSWI *SwitchDiscreteControlXSWI `protobuf:"bytes,3,opt,name=switchDiscreteControlXSWI,proto3" json:"switchDiscreteControlXSWI,omitempty"`
	XXX_NoUnkeyedLiteral      struct{}                   `json:"-"`
	XXX_unrecognized          []byte                     `json:"-"`
	XXX_sizecache             int32                      `json:"-"`
}

func (m *SwitchDiscreteControl) Reset()         { *m = SwitchDiscreteControl{} }
func (m *SwitchDiscreteControl) String() string { return proto.CompactTextString(m) }
func (*SwitchDiscreteControl) ProtoMessage()    {}
func (*SwitchDiscreteControl) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{1}
}

func (m *SwitchDiscreteControl) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchDiscreteControl.Unmarshal(m, b)
}
func (m *SwitchDiscreteControl) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchDiscreteControl.Marshal(b, m, deterministic)
}
func (m *SwitchDiscreteControl) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchDiscreteControl.Merge(m, src)
}
func (m *SwitchDiscreteControl) XXX_Size() int {
	return xxx_messageInfo_SwitchDiscreteControl.Size(m)
}
func (m *SwitchDiscreteControl) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchDiscreteControl.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchDiscreteControl proto.InternalMessageInfo

func (m *SwitchDiscreteControl) GetControlValue() *commonmodule.ControlValue {
	if m != nil {
		return m.ControlValue
	}
	return nil
}

func (m *SwitchDiscreteControl) GetCheck() *commonmodule.CheckConditions {
	if m != nil {
		return m.Check
	}
	return nil
}

func (m *SwitchDiscreteControl) GetSwitchDiscreteControlXSWI() *SwitchDiscreteControlXSWI {
	if m != nil {
		return m.SwitchDiscreteControlXSWI
	}
	return nil
}

// A ProtectedSwitch is a switching device that can be operated by ProtectionEquipment.
type ProtectedSwitch struct {
	// UML inherited base object
	ConductingEquipment  *commonmodule.ConductingEquipment `protobuf:"bytes,1,opt,name=conductingEquipment,proto3" json:"conductingEquipment,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                          `json:"-"`
	XXX_unrecognized     []byte                            `json:"-"`
	XXX_sizecache        int32                             `json:"-"`
}

func (m *ProtectedSwitch) Reset()         { *m = ProtectedSwitch{} }
func (m *ProtectedSwitch) String() string { return proto.CompactTextString(m) }
func (*ProtectedSwitch) ProtoMessage()    {}
func (*ProtectedSwitch) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{2}
}

func (m *ProtectedSwitch) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_ProtectedSwitch.Unmarshal(m, b)
}
func (m *ProtectedSwitch) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_ProtectedSwitch.Marshal(b, m, deterministic)
}
func (m *ProtectedSwitch) XXX_Merge(src proto.Message) {
	xxx_messageInfo_ProtectedSwitch.Merge(m, src)
}
func (m *ProtectedSwitch) XXX_Size() int {
	return xxx_messageInfo_ProtectedSwitch.Size(m)
}
func (m *ProtectedSwitch) XXX_DiscardUnknown() {
	xxx_messageInfo_ProtectedSwitch.DiscardUnknown(m)
}

var xxx_messageInfo_ProtectedSwitch proto.InternalMessageInfo

func (m *ProtectedSwitch) GetConductingEquipment() *commonmodule.ConductingEquipment {
	if m != nil {
		return m.ConductingEquipment
	}
	return nil
}

// Switch control profile
type SwitchDiscreteControlProfile struct {
	// UML inherited base object
	ControlMessageInfo *commonmodule.ControlMessageInfo `protobuf:"bytes,1,opt,name=controlMessageInfo,proto3" json:"controlMessageInfo,omitempty"`
	// MISSING DOCUMENTATION!!!
	Ied *commonmodule.IED `protobuf:"bytes,2,opt,name=ied,proto3" json:"ied,omitempty"`
	// MISSING DOCUMENTATION!!!
	ProtectedSwitch *ProtectedSwitch `protobuf:"bytes,3,opt,name=protectedSwitch,proto3" json:"protectedSwitch,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchDiscreteControl *SwitchDiscreteControl `protobuf:"bytes,4,opt,name=switchDiscreteControl,proto3" json:"switchDiscreteControl,omitempty"`
	XXX_NoUnkeyedLiteral  struct{}               `json:"-"`
	XXX_unrecognized      []byte                 `json:"-"`
	XXX_sizecache         int32                  `json:"-"`
}

func (m *SwitchDiscreteControlProfile) Reset()         { *m = SwitchDiscreteControlProfile{} }
func (m *SwitchDiscreteControlProfile) String() string { return proto.CompactTextString(m) }
func (*SwitchDiscreteControlProfile) ProtoMessage()    {}
func (*SwitchDiscreteControlProfile) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{3}
}

func (m *SwitchDiscreteControlProfile) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchDiscreteControlProfile.Unmarshal(m, b)
}
func (m *SwitchDiscreteControlProfile) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchDiscreteControlProfile.Marshal(b, m, deterministic)
}
func (m *SwitchDiscreteControlProfile) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchDiscreteControlProfile.Merge(m, src)
}
func (m *SwitchDiscreteControlProfile) XXX_Size() int {
	return xxx_messageInfo_SwitchDiscreteControlProfile.Size(m)
}
func (m *SwitchDiscreteControlProfile) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchDiscreteControlProfile.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchDiscreteControlProfile proto.InternalMessageInfo

func (m *SwitchDiscreteControlProfile) GetControlMessageInfo() *commonmodule.ControlMessageInfo {
	if m != nil {
		return m.ControlMessageInfo
	}
	return nil
}

func (m *SwitchDiscreteControlProfile) GetIed() *commonmodule.IED {
	if m != nil {
		return m.Ied
	}
	return nil
}

func (m *SwitchDiscreteControlProfile) GetProtectedSwitch() *ProtectedSwitch {
	if m != nil {
		return m.ProtectedSwitch
	}
	return nil
}

func (m *SwitchDiscreteControlProfile) GetSwitchDiscreteControl() *SwitchDiscreteControl {
	if m != nil {
		return m.SwitchDiscreteControl
	}
	return nil
}

// OpenFMB specialization for SwitchEventProfile
type SwitchEventXSWI struct {
	// UML inherited base object
	LogicalNodeForEventAndStatus *commonmodule.LogicalNodeForEventAndStatus `protobuf:"bytes,1,opt,name=logicalNodeForEventAndStatus,proto3" json:"logicalNodeForEventAndStatus,omitempty"`
	// Dynamic test status
	DynamicTest *commonmodule.ENS_DynamicTestKind `protobuf:"bytes,2,opt,name=DynamicTest,proto3" json:"DynamicTest,omitempty"`
	// MISSING DOCUMENTATION!!!
	Pos                  *commonmodule.StatusDPS `protobuf:"bytes,3,opt,name=Pos,proto3" json:"Pos,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                `json:"-"`
	XXX_unrecognized     []byte                  `json:"-"`
	XXX_sizecache        int32                   `json:"-"`
}

func (m *SwitchEventXSWI) Reset()         { *m = SwitchEventXSWI{} }
func (m *SwitchEventXSWI) String() string { return proto.CompactTextString(m) }
func (*SwitchEventXSWI) ProtoMessage()    {}
func (*SwitchEventXSWI) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{4}
}

func (m *SwitchEventXSWI) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchEventXSWI.Unmarshal(m, b)
}
func (m *SwitchEventXSWI) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchEventXSWI.Marshal(b, m, deterministic)
}
func (m *SwitchEventXSWI) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchEventXSWI.Merge(m, src)
}
func (m *SwitchEventXSWI) XXX_Size() int {
	return xxx_messageInfo_SwitchEventXSWI.Size(m)
}
func (m *SwitchEventXSWI) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchEventXSWI.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchEventXSWI proto.InternalMessageInfo

func (m *SwitchEventXSWI) GetLogicalNodeForEventAndStatus() *commonmodule.LogicalNodeForEventAndStatus {
	if m != nil {
		return m.LogicalNodeForEventAndStatus
	}
	return nil
}

func (m *SwitchEventXSWI) GetDynamicTest() *commonmodule.ENS_DynamicTestKind {
	if m != nil {
		return m.DynamicTest
	}
	return nil
}

func (m *SwitchEventXSWI) GetPos() *commonmodule.StatusDPS {
	if m != nil {
		return m.Pos
	}
	return nil
}

// Switch event
type SwitchEvent struct {
	// UML inherited base object
	EventValue *commonmodule.EventValue `protobuf:"bytes,1,opt,name=eventValue,proto3" json:"eventValue,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchEventXSWI      *SwitchEventXSWI `protobuf:"bytes,2,opt,name=switchEventXSWI,proto3" json:"switchEventXSWI,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *SwitchEvent) Reset()         { *m = SwitchEvent{} }
func (m *SwitchEvent) String() string { return proto.CompactTextString(m) }
func (*SwitchEvent) ProtoMessage()    {}
func (*SwitchEvent) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{5}
}

func (m *SwitchEvent) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchEvent.Unmarshal(m, b)
}
func (m *SwitchEvent) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchEvent.Marshal(b, m, deterministic)
}
func (m *SwitchEvent) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchEvent.Merge(m, src)
}
func (m *SwitchEvent) XXX_Size() int {
	return xxx_messageInfo_SwitchEvent.Size(m)
}
func (m *SwitchEvent) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchEvent.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchEvent proto.InternalMessageInfo

func (m *SwitchEvent) GetEventValue() *commonmodule.EventValue {
	if m != nil {
		return m.EventValue
	}
	return nil
}

func (m *SwitchEvent) GetSwitchEventXSWI() *SwitchEventXSWI {
	if m != nil {
		return m.SwitchEventXSWI
	}
	return nil
}

// Switch event profile
type SwitchEventProfile struct {
	// UML inherited base object
	EventMessageInfo *commonmodule.EventMessageInfo `protobuf:"bytes,1,opt,name=eventMessageInfo,proto3" json:"eventMessageInfo,omitempty"`
	// MISSING DOCUMENTATION!!!
	Ied *commonmodule.IED `protobuf:"bytes,2,opt,name=ied,proto3" json:"ied,omitempty"`
	// MISSING DOCUMENTATION!!!
	ProtectedSwitch *ProtectedSwitch `protobuf:"bytes,3,opt,name=protectedSwitch,proto3" json:"protectedSwitch,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchEvent          *SwitchEvent `protobuf:"bytes,4,opt,name=switchEvent,proto3" json:"switchEvent,omitempty"`
	XXX_NoUnkeyedLiteral struct{}     `json:"-"`
	XXX_unrecognized     []byte       `json:"-"`
	XXX_sizecache        int32        `json:"-"`
}

func (m *SwitchEventProfile) Reset()         { *m = SwitchEventProfile{} }
func (m *SwitchEventProfile) String() string { return proto.CompactTextString(m) }
func (*SwitchEventProfile) ProtoMessage()    {}
func (*SwitchEventProfile) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{6}
}

func (m *SwitchEventProfile) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchEventProfile.Unmarshal(m, b)
}
func (m *SwitchEventProfile) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchEventProfile.Marshal(b, m, deterministic)
}
func (m *SwitchEventProfile) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchEventProfile.Merge(m, src)
}
func (m *SwitchEventProfile) XXX_Size() int {
	return xxx_messageInfo_SwitchEventProfile.Size(m)
}
func (m *SwitchEventProfile) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchEventProfile.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchEventProfile proto.InternalMessageInfo

func (m *SwitchEventProfile) GetEventMessageInfo() *commonmodule.EventMessageInfo {
	if m != nil {
		return m.EventMessageInfo
	}
	return nil
}

func (m *SwitchEventProfile) GetIed() *commonmodule.IED {
	if m != nil {
		return m.Ied
	}
	return nil
}

func (m *SwitchEventProfile) GetProtectedSwitch() *ProtectedSwitch {
	if m != nil {
		return m.ProtectedSwitch
	}
	return nil
}

func (m *SwitchEventProfile) GetSwitchEvent() *SwitchEvent {
	if m != nil {
		return m.SwitchEvent
	}
	return nil
}

// Switch reading value
type SwitchReading struct {
	// UML inherited base object
	ConductingEquipmentTerminalReading *commonmodule.ConductingEquipmentTerminalReading `protobuf:"bytes,1,opt,name=conductingEquipmentTerminalReading,proto3" json:"conductingEquipmentTerminalReading,omitempty"`
	// MISSING DOCUMENTATION!!!
	DiffReadingMMXU *commonmodule.ReadingMMXU `protobuf:"bytes,2,opt,name=diffReadingMMXU,proto3" json:"diffReadingMMXU,omitempty"`
	// MISSING DOCUMENTATION!!!
	PhaseMMTN *commonmodule.PhaseMMTN `protobuf:"bytes,3,opt,name=phaseMMTN,proto3" json:"phaseMMTN,omitempty"`
	// MISSING DOCUMENTATION!!!
	ReadingMMTR *commonmodule.ReadingMMTR `protobuf:"bytes,4,opt,name=readingMMTR,proto3" json:"readingMMTR,omitempty"`
	// MISSING DOCUMENTATION!!!
	ReadingMMXU          *commonmodule.ReadingMMXU `protobuf:"bytes,5,opt,name=readingMMXU,proto3" json:"readingMMXU,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                  `json:"-"`
	XXX_unrecognized     []byte                    `json:"-"`
	XXX_sizecache        int32                     `json:"-"`
}

func (m *SwitchReading) Reset()         { *m = SwitchReading{} }
func (m *SwitchReading) String() string { return proto.CompactTextString(m) }
func (*SwitchReading) ProtoMessage()    {}
func (*SwitchReading) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{7}
}

func (m *SwitchReading) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchReading.Unmarshal(m, b)
}
func (m *SwitchReading) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchReading.Marshal(b, m, deterministic)
}
func (m *SwitchReading) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchReading.Merge(m, src)
}
func (m *SwitchReading) XXX_Size() int {
	return xxx_messageInfo_SwitchReading.Size(m)
}
func (m *SwitchReading) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchReading.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchReading proto.InternalMessageInfo

func (m *SwitchReading) GetConductingEquipmentTerminalReading() *commonmodule.ConductingEquipmentTerminalReading {
	if m != nil {
		return m.ConductingEquipmentTerminalReading
	}
	return nil
}

func (m *SwitchReading) GetDiffReadingMMXU() *commonmodule.ReadingMMXU {
	if m != nil {
		return m.DiffReadingMMXU
	}
	return nil
}

func (m *SwitchReading) GetPhaseMMTN() *commonmodule.PhaseMMTN {
	if m != nil {
		return m.PhaseMMTN
	}
	return nil
}

func (m *SwitchReading) GetReadingMMTR() *commonmodule.ReadingMMTR {
	if m != nil {
		return m.ReadingMMTR
	}
	return nil
}

func (m *SwitchReading) GetReadingMMXU() *commonmodule.ReadingMMXU {
	if m != nil {
		return m.ReadingMMXU
	}
	return nil
}

// Switch reading profile
type SwitchReadingProfile struct {
	// UML inherited base object
	ReadingMessageInfo *commonmodule.ReadingMessageInfo `protobuf:"bytes,1,opt,name=readingMessageInfo,proto3" json:"readingMessageInfo,omitempty"`
	// MISSING DOCUMENTATION!!!
	Ied *commonmodule.IED `protobuf:"bytes,2,opt,name=ied,proto3" json:"ied,omitempty"`
	// MISSING DOCUMENTATION!!!
	ProtectedSwitch *ProtectedSwitch `protobuf:"bytes,3,opt,name=protectedSwitch,proto3" json:"protectedSwitch,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchReading        []*SwitchReading `protobuf:"bytes,4,rep,name=switchReading,proto3" json:"switchReading,omitempty"`
	XXX_NoUnkeyedLiteral struct{}         `json:"-"`
	XXX_unrecognized     []byte           `json:"-"`
	XXX_sizecache        int32            `json:"-"`
}

func (m *SwitchReadingProfile) Reset()         { *m = SwitchReadingProfile{} }
func (m *SwitchReadingProfile) String() string { return proto.CompactTextString(m) }
func (*SwitchReadingProfile) ProtoMessage()    {}
func (*SwitchReadingProfile) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{8}
}

func (m *SwitchReadingProfile) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchReadingProfile.Unmarshal(m, b)
}
func (m *SwitchReadingProfile) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchReadingProfile.Marshal(b, m, deterministic)
}
func (m *SwitchReadingProfile) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchReadingProfile.Merge(m, src)
}
func (m *SwitchReadingProfile) XXX_Size() int {
	return xxx_messageInfo_SwitchReadingProfile.Size(m)
}
func (m *SwitchReadingProfile) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchReadingProfile.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchReadingProfile proto.InternalMessageInfo

func (m *SwitchReadingProfile) GetReadingMessageInfo() *commonmodule.ReadingMessageInfo {
	if m != nil {
		return m.ReadingMessageInfo
	}
	return nil
}

func (m *SwitchReadingProfile) GetIed() *commonmodule.IED {
	if m != nil {
		return m.Ied
	}
	return nil
}

func (m *SwitchReadingProfile) GetProtectedSwitch() *ProtectedSwitch {
	if m != nil {
		return m.ProtectedSwitch
	}
	return nil
}

func (m *SwitchReadingProfile) GetSwitchReading() []*SwitchReading {
	if m != nil {
		return m.SwitchReading
	}
	return nil
}

// OpenFMB specialization for SwitchStatusProfile
type SwitchStatusXSWI struct {
	// UML inherited base object
	LogicalNodeForEventAndStatus *commonmodule.LogicalNodeForEventAndStatus `protobuf:"bytes,1,opt,name=logicalNodeForEventAndStatus,proto3" json:"logicalNodeForEventAndStatus,omitempty"`
	// MISSING DOCUMENTATION!!!
	DynamicTest *commonmodule.ENS_DynamicTestKind `protobuf:"bytes,2,opt,name=DynamicTest,proto3" json:"DynamicTest,omitempty"`
	// MISSING DOCUMENTATION!!!
	Pos                  *commonmodule.StatusDPS `protobuf:"bytes,3,opt,name=Pos,proto3" json:"Pos,omitempty"`
	XXX_NoUnkeyedLiteral struct{}                `json:"-"`
	XXX_unrecognized     []byte                  `json:"-"`
	XXX_sizecache        int32                   `json:"-"`
}

func (m *SwitchStatusXSWI) Reset()         { *m = SwitchStatusXSWI{} }
func (m *SwitchStatusXSWI) String() string { return proto.CompactTextString(m) }
func (*SwitchStatusXSWI) ProtoMessage()    {}
func (*SwitchStatusXSWI) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{9}
}

func (m *SwitchStatusXSWI) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchStatusXSWI.Unmarshal(m, b)
}
func (m *SwitchStatusXSWI) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchStatusXSWI.Marshal(b, m, deterministic)
}
func (m *SwitchStatusXSWI) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchStatusXSWI.Merge(m, src)
}
func (m *SwitchStatusXSWI) XXX_Size() int {
	return xxx_messageInfo_SwitchStatusXSWI.Size(m)
}
func (m *SwitchStatusXSWI) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchStatusXSWI.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchStatusXSWI proto.InternalMessageInfo

func (m *SwitchStatusXSWI) GetLogicalNodeForEventAndStatus() *commonmodule.LogicalNodeForEventAndStatus {
	if m != nil {
		return m.LogicalNodeForEventAndStatus
	}
	return nil
}

func (m *SwitchStatusXSWI) GetDynamicTest() *commonmodule.ENS_DynamicTestKind {
	if m != nil {
		return m.DynamicTest
	}
	return nil
}

func (m *SwitchStatusXSWI) GetPos() *commonmodule.StatusDPS {
	if m != nil {
		return m.Pos
	}
	return nil
}

// Switch status
type SwitchStatus struct {
	// UML inherited base object
	StatusValue *commonmodule.StatusValue `protobuf:"bytes,1,opt,name=statusValue,proto3" json:"statusValue,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchStatusXSWI     *SwitchStatusXSWI `protobuf:"bytes,2,opt,name=switchStatusXSWI,proto3" json:"switchStatusXSWI,omitempty"`
	XXX_NoUnkeyedLiteral struct{}          `json:"-"`
	XXX_unrecognized     []byte            `json:"-"`
	XXX_sizecache        int32             `json:"-"`
}

func (m *SwitchStatus) Reset()         { *m = SwitchStatus{} }
func (m *SwitchStatus) String() string { return proto.CompactTextString(m) }
func (*SwitchStatus) ProtoMessage()    {}
func (*SwitchStatus) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{10}
}

func (m *SwitchStatus) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchStatus.Unmarshal(m, b)
}
func (m *SwitchStatus) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchStatus.Marshal(b, m, deterministic)
}
func (m *SwitchStatus) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchStatus.Merge(m, src)
}
func (m *SwitchStatus) XXX_Size() int {
	return xxx_messageInfo_SwitchStatus.Size(m)
}
func (m *SwitchStatus) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchStatus.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchStatus proto.InternalMessageInfo

func (m *SwitchStatus) GetStatusValue() *commonmodule.StatusValue {
	if m != nil {
		return m.StatusValue
	}
	return nil
}

func (m *SwitchStatus) GetSwitchStatusXSWI() *SwitchStatusXSWI {
	if m != nil {
		return m.SwitchStatusXSWI
	}
	return nil
}

// Switch status profile
type SwitchStatusProfile struct {
	// UML inherited base object
	StatusMessageInfo *commonmodule.StatusMessageInfo `protobuf:"bytes,1,opt,name=statusMessageInfo,proto3" json:"statusMessageInfo,omitempty"`
	// MISSING DOCUMENTATION!!!
	Ied *commonmodule.IED `protobuf:"bytes,2,opt,name=ied,proto3" json:"ied,omitempty"`
	// MISSING DOCUMENTATION!!!
	ProtectedSwitch *ProtectedSwitch `protobuf:"bytes,3,opt,name=protectedSwitch,proto3" json:"protectedSwitch,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchStatus         *SwitchStatus `protobuf:"bytes,4,opt,name=switchStatus,proto3" json:"switchStatus,omitempty"`
	XXX_NoUnkeyedLiteral struct{}      `json:"-"`
	XXX_unrecognized     []byte        `json:"-"`
	XXX_sizecache        int32         `json:"-"`
}

func (m *SwitchStatusProfile) Reset()         { *m = SwitchStatusProfile{} }
func (m *SwitchStatusProfile) String() string { return proto.CompactTextString(m) }
func (*SwitchStatusProfile) ProtoMessage()    {}
func (*SwitchStatusProfile) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{11}
}

func (m *SwitchStatusProfile) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchStatusProfile.Unmarshal(m, b)
}
func (m *SwitchStatusProfile) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchStatusProfile.Marshal(b, m, deterministic)
}
func (m *SwitchStatusProfile) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchStatusProfile.Merge(m, src)
}
func (m *SwitchStatusProfile) XXX_Size() int {
	return xxx_messageInfo_SwitchStatusProfile.Size(m)
}
func (m *SwitchStatusProfile) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchStatusProfile.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchStatusProfile proto.InternalMessageInfo

func (m *SwitchStatusProfile) GetStatusMessageInfo() *commonmodule.StatusMessageInfo {
	if m != nil {
		return m.StatusMessageInfo
	}
	return nil
}

func (m *SwitchStatusProfile) GetIed() *commonmodule.IED {
	if m != nil {
		return m.Ied
	}
	return nil
}

func (m *SwitchStatusProfile) GetProtectedSwitch() *ProtectedSwitch {
	if m != nil {
		return m.ProtectedSwitch
	}
	return nil
}

func (m *SwitchStatusProfile) GetSwitchStatus() *SwitchStatus {
	if m != nil {
		return m.SwitchStatus
	}
	return nil
}

// Specialized 61850 FSCC class.  LN: Schedule controller   Name: FSCC
type SwitchControlFSCC struct {
	// UML inherited base object
	LogicalNodeForControl *commonmodule.LogicalNodeForControl `protobuf:"bytes,1,opt,name=logicalNodeForControl,proto3" json:"logicalNodeForControl,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchControlScheduleFSCH *commonmodule.SwitchControlScheduleFSCH `protobuf:"bytes,2,opt,name=switchControlScheduleFSCH,proto3" json:"switchControlScheduleFSCH,omitempty"`
	XXX_NoUnkeyedLiteral      struct{}                                `json:"-"`
	XXX_unrecognized          []byte                                  `json:"-"`
	XXX_sizecache             int32                                   `json:"-"`
}

func (m *SwitchControlFSCC) Reset()         { *m = SwitchControlFSCC{} }
func (m *SwitchControlFSCC) String() string { return proto.CompactTextString(m) }
func (*SwitchControlFSCC) ProtoMessage()    {}
func (*SwitchControlFSCC) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{12}
}

func (m *SwitchControlFSCC) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchControlFSCC.Unmarshal(m, b)
}
func (m *SwitchControlFSCC) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchControlFSCC.Marshal(b, m, deterministic)
}
func (m *SwitchControlFSCC) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchControlFSCC.Merge(m, src)
}
func (m *SwitchControlFSCC) XXX_Size() int {
	return xxx_messageInfo_SwitchControlFSCC.Size(m)
}
func (m *SwitchControlFSCC) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchControlFSCC.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchControlFSCC proto.InternalMessageInfo

func (m *SwitchControlFSCC) GetLogicalNodeForControl() *commonmodule.LogicalNodeForControl {
	if m != nil {
		return m.LogicalNodeForControl
	}
	return nil
}

func (m *SwitchControlFSCC) GetSwitchControlScheduleFSCH() *commonmodule.SwitchControlScheduleFSCH {
	if m != nil {
		return m.SwitchControlScheduleFSCH
	}
	return nil
}

// Switch discrete control
type SwitchControl struct {
	// UML inherited base object
	ControlValue *commonmodule.ControlValue `protobuf:"bytes,1,opt,name=controlValue,proto3" json:"controlValue,omitempty"`
	// MISSING DOCUMENTATION!!!
	Check *commonmodule.CheckConditions `protobuf:"bytes,2,opt,name=check,proto3" json:"check,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchControlFSCC    *SwitchControlFSCC `protobuf:"bytes,3,opt,name=SwitchControlFSCC,proto3" json:"SwitchControlFSCC,omitempty"`
	XXX_NoUnkeyedLiteral struct{}           `json:"-"`
	XXX_unrecognized     []byte             `json:"-"`
	XXX_sizecache        int32              `json:"-"`
}

func (m *SwitchControl) Reset()         { *m = SwitchControl{} }
func (m *SwitchControl) String() string { return proto.CompactTextString(m) }
func (*SwitchControl) ProtoMessage()    {}
func (*SwitchControl) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{13}
}

func (m *SwitchControl) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchControl.Unmarshal(m, b)
}
func (m *SwitchControl) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchControl.Marshal(b, m, deterministic)
}
func (m *SwitchControl) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchControl.Merge(m, src)
}
func (m *SwitchControl) XXX_Size() int {
	return xxx_messageInfo_SwitchControl.Size(m)
}
func (m *SwitchControl) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchControl.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchControl proto.InternalMessageInfo

func (m *SwitchControl) GetControlValue() *commonmodule.ControlValue {
	if m != nil {
		return m.ControlValue
	}
	return nil
}

func (m *SwitchControl) GetCheck() *commonmodule.CheckConditions {
	if m != nil {
		return m.Check
	}
	return nil
}

func (m *SwitchControl) GetSwitchControlFSCC() *SwitchControlFSCC {
	if m != nil {
		return m.SwitchControlFSCC
	}
	return nil
}

// Switch control profile
type SwitchControlProfile struct {
	// UML inherited base object
	ControlMessageInfo *commonmodule.ControlMessageInfo `protobuf:"bytes,1,opt,name=controlMessageInfo,proto3" json:"controlMessageInfo,omitempty"`
	// MISSING DOCUMENTATION!!!
	Ied *commonmodule.IED `protobuf:"bytes,2,opt,name=ied,proto3" json:"ied,omitempty"`
	// MISSING DOCUMENTATION!!!
	ProtectedSwitch *ProtectedSwitch `protobuf:"bytes,3,opt,name=protectedSwitch,proto3" json:"protectedSwitch,omitempty"`
	// MISSING DOCUMENTATION!!!
	SwitchControl        *SwitchControl `protobuf:"bytes,4,opt,name=switchControl,proto3" json:"switchControl,omitempty"`
	XXX_NoUnkeyedLiteral struct{}       `json:"-"`
	XXX_unrecognized     []byte         `json:"-"`
	XXX_sizecache        int32          `json:"-"`
}

func (m *SwitchControlProfile) Reset()         { *m = SwitchControlProfile{} }
func (m *SwitchControlProfile) String() string { return proto.CompactTextString(m) }
func (*SwitchControlProfile) ProtoMessage()    {}
func (*SwitchControlProfile) Descriptor() ([]byte, []int) {
	return fileDescriptor_a3060f36f03b5f33, []int{14}
}

func (m *SwitchControlProfile) XXX_Unmarshal(b []byte) error {
	return xxx_messageInfo_SwitchControlProfile.Unmarshal(m, b)
}
func (m *SwitchControlProfile) XXX_Marshal(b []byte, deterministic bool) ([]byte, error) {
	return xxx_messageInfo_SwitchControlProfile.Marshal(b, m, deterministic)
}
func (m *SwitchControlProfile) XXX_Merge(src proto.Message) {
	xxx_messageInfo_SwitchControlProfile.Merge(m, src)
}
func (m *SwitchControlProfile) XXX_Size() int {
	return xxx_messageInfo_SwitchControlProfile.Size(m)
}
func (m *SwitchControlProfile) XXX_DiscardUnknown() {
	xxx_messageInfo_SwitchControlProfile.DiscardUnknown(m)
}

var xxx_messageInfo_SwitchControlProfile proto.InternalMessageInfo

func (m *SwitchControlProfile) GetControlMessageInfo() *commonmodule.ControlMessageInfo {
	if m != nil {
		return m.ControlMessageInfo
	}
	return nil
}

func (m *SwitchControlProfile) GetIed() *commonmodule.IED {
	if m != nil {
		return m.Ied
	}
	return nil
}

func (m *SwitchControlProfile) GetProtectedSwitch() *ProtectedSwitch {
	if m != nil {
		return m.ProtectedSwitch
	}
	return nil
}

func (m *SwitchControlProfile) GetSwitchControl() *SwitchControl {
	if m != nil {
		return m.SwitchControl
	}
	return nil
}

func init() {
	proto.RegisterType((*SwitchDiscreteControlXSWI)(nil), "switchmodule.SwitchDiscreteControlXSWI")
	proto.RegisterType((*SwitchDiscreteControl)(nil), "switchmodule.SwitchDiscreteControl")
	proto.RegisterType((*ProtectedSwitch)(nil), "switchmodule.ProtectedSwitch")
	proto.RegisterType((*SwitchDiscreteControlProfile)(nil), "switchmodule.SwitchDiscreteControlProfile")
	proto.RegisterType((*SwitchEventXSWI)(nil), "switchmodule.SwitchEventXSWI")
	proto.RegisterType((*SwitchEvent)(nil), "switchmodule.SwitchEvent")
	proto.RegisterType((*SwitchEventProfile)(nil), "switchmodule.SwitchEventProfile")
	proto.RegisterType((*SwitchReading)(nil), "switchmodule.SwitchReading")
	proto.RegisterType((*SwitchReadingProfile)(nil), "switchmodule.SwitchReadingProfile")
	proto.RegisterType((*SwitchStatusXSWI)(nil), "switchmodule.SwitchStatusXSWI")
	proto.RegisterType((*SwitchStatus)(nil), "switchmodule.SwitchStatus")
	proto.RegisterType((*SwitchStatusProfile)(nil), "switchmodule.SwitchStatusProfile")
	proto.RegisterType((*SwitchControlFSCC)(nil), "switchmodule.SwitchControlFSCC")
	proto.RegisterType((*SwitchControl)(nil), "switchmodule.SwitchControl")
	proto.RegisterType((*SwitchControlProfile)(nil), "switchmodule.SwitchControlProfile")
}

func init() { proto.RegisterFile("switchmodule/switchmodule.proto", fileDescriptor_a3060f36f03b5f33) }

var fileDescriptor_a3060f36f03b5f33 = []byte{
	// 983 bytes of a gzipped FileDescriptorProto
	0x1f, 0x8b, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x02, 0xff, 0xe4, 0x57, 0x4f, 0x6f, 0xdc, 0xd4,
	0x17, 0x95, 0x3d, 0xe9, 0x4f, 0xbf, 0xde, 0x99, 0x2a, 0xc9, 0x6b, 0x23, 0x3c, 0xe9, 0x9f, 0x04,
	0xb3, 0x68, 0x55, 0x29, 0x19, 0xd4, 0x88, 0x0d, 0x48, 0x48, 0xcd, 0xcc, 0x44, 0x8d, 0xca, 0x04,
	0x63, 0x4f, 0xdb, 0xc0, 0xa6, 0x72, 0xec, 0x37, 0x13, 0x0b, 0x8f, 0xdf, 0x30, 0xcf, 0x03, 0x62,
	0x47, 0x41, 0x42, 0xac, 0x10, 0x4b, 0x96, 0xec, 0x40, 0xec, 0xb3, 0x63, 0x81, 0xc4, 0x82, 0x0f,
	0xc1, 0xb2, 0x1f, 0x81, 0x2f, 0x80, 0x6c, 0x5f, 0x4f, 0xef, 0xb3, 0xdf, 0x24, 0x6c, 0x90, 0x22,
	0x75, 0xe7, 0xf1, 0x3b, 0xe7, 0xbc, 0xe7, 0x73, 0xcf, 0xbd, 0x63, 0xc3, 0x96, 0xfc, 0x22, 0x4a,
	0x83, 0xd3, 0x89, 0x08, 0xe7, 0x31, 0xef, 0xd0, 0x1f, 0xbb, 0xd3, 0x99, 0x48, 0x05, 0x6b, 0xd1,
	0x7b, 0x9b, 0x57, 0xe7, 0x93, 0xb8, 0x58, 0xd8, 0xdc, 0x0a, 0xc4, 0x64, 0x22, 0x12, 0x64, 0xd2,
	0x1f, 0x05, 0xc0, 0xfe, 0xc5, 0x80, 0xb6, 0x97, 0x93, 0x7b, 0x91, 0x0c, 0x66, 0x3c, 0xe5, 0x5d,
	0x91, 0xa4, 0x33, 0x11, 0x1f, 0x7b, 0xcf, 0x0e, 0xd9, 0x73, 0xd8, 0x88, 0xc5, 0x38, 0x0a, 0xfc,
	0xf8, 0x48, 0x84, 0xfc, 0x40, 0xcc, 0x70, 0xd1, 0x32, 0xb6, 0x8d, 0x7b, 0xcd, 0x07, 0x6f, 0xed,
	0x2a, 0x8a, 0x1f, 0xe8, 0xa0, 0xfb, 0x2b, 0x5f, 0x9d, 0x59, 0x86, 0xab, 0xd7, 0x61, 0xf7, 0xa1,
	0xe1, 0x08, 0x69, 0x99, 0xb9, 0x9c, 0xa5, 0xca, 0x21, 0xa6, 0xe7, 0x74, 0xdd, 0x0c, 0x64, 0x7f,
	0x63, 0xc2, 0x86, 0xf6, 0xa8, 0xac, 0x07, 0xad, 0xa0, 0xb8, 0x7c, 0xea, 0xc7, 0x73, 0x8e, 0xa7,
	0xdb, 0xd4, 0xca, 0xe5, 0x08, 0x3c, 0x94, 0xc2, 0x62, 0x7b, 0x70, 0x25, 0x38, 0xe5, 0xc1, 0xa7,
	0x78, 0x9a, 0xdb, 0x15, 0x7a, 0xb6, 0xd4, 0x15, 0x49, 0x18, 0xa5, 0x91, 0x48, 0xa4, 0x5b, 0x60,
	0x99, 0x80, 0xb6, 0x5c, 0x66, 0x9f, 0xd5, 0xc8, 0x85, 0xee, 0xee, 0x2a, 0x15, 0x5b, 0xea, 0xf6,
	0xfe, 0xff, 0xbf, 0x3b, 0xb3, 0x8c, 0x1f, 0xb2, 0x83, 0x2d, 0xd7, 0xb4, 0x63, 0x58, 0x75, 0x66,
	0x22, 0xe5, 0x41, 0xca, 0xc3, 0x42, 0x8a, 0x7d, 0x0c, 0xd7, 0x03, 0x91, 0x84, 0xf3, 0x20, 0x8d,
	0x92, 0x71, 0xff, 0xb3, 0x79, 0x34, 0x9d, 0xf0, 0x24, 0x45, 0x17, 0xde, 0xac, 0xb9, 0x50, 0x05,
	0xa2, 0x19, 0x3a, 0x0d, 0xfb, 0xa5, 0x09, 0xb7, 0xb4, 0x07, 0x76, 0x66, 0x62, 0x14, 0xc5, 0x9c,
	0x3d, 0x05, 0x86, 0x26, 0x0e, 0xb8, 0x94, 0xfe, 0x98, 0x1f, 0x26, 0x23, 0x81, 0x5b, 0x6f, 0x6b,
	0x0b, 0x40, 0x70, 0xb8, 0xb3, 0x46, 0x81, 0xed, 0x40, 0x23, 0xe2, 0x21, 0x96, 0x62, 0x5d, 0x15,
	0x3a, 0xec, 0xf7, 0x88, 0x57, 0x19, 0x8e, 0x7d, 0x04, 0xab, 0x53, 0xd5, 0x15, 0x34, 0xff, 0xb6,
	0x6a, 0x7e, 0xc5, 0x3a, 0x22, 0x53, 0xe5, 0xb3, 0x00, 0x36, 0xb4, 0x55, 0xb0, 0x56, 0x30, 0xfb,
	0x17, 0x57, 0x95, 0xc8, 0xeb, 0xb5, 0xde, 0x5d, 0xf9, 0xfd, 0x6f, 0xcb, 0xb0, 0x5f, 0x98, 0xb0,
	0x5a, 0x08, 0xf4, 0x3f, 0xe7, 0x49, 0x9a, 0xb7, 0x5e, 0x0a, 0xb7, 0xd4, 0x96, 0xc9, 0x97, 0x1e,
	0x26, 0xa1, 0x97, 0xfa, 0xe9, 0x5c, 0xa2, 0xc5, 0xf7, 0xcf, 0xeb, 0x40, 0x95, 0x81, 0x66, 0x9f,
	0xab, 0xca, 0xba, 0xd0, 0xec, 0x7d, 0x99, 0xf8, 0x93, 0x28, 0x18, 0x72, 0x99, 0xa2, 0xfd, 0x95,
	0x08, 0xf5, 0x8f, 0xbc, 0xe7, 0x04, 0xf4, 0x38, 0x4a, 0x42, 0x97, 0xb2, 0xd8, 0x5e, 0xd1, 0xd4,
	0x45, 0x01, 0xde, 0x50, 0xc9, 0xc5, 0x3e, 0x3d, 0xc7, 0xa3, 0x15, 0xcc, 0xba, 0xfb, 0x27, 0x03,
	0x9a, 0xc4, 0x03, 0xf6, 0x3e, 0x00, 0xcf, 0x2e, 0x68, 0x47, 0x57, 0x06, 0x44, 0x7f, 0xb1, 0x8e,
	0xcf, 0x46, 0x18, 0x59, 0x22, 0xa4, 0x6a, 0xe9, 0xa2, 0xaf, 0x35, 0x85, 0x5b, 0x80, 0x68, 0x22,
	0x2a, 0x7c, 0xfb, 0xcc, 0x04, 0x46, 0xe0, 0x65, 0x0b, 0x38, 0xb0, 0x96, 0xef, 0x5b, 0x6f, 0x80,
	0x3b, 0x9a, 0xf3, 0xd6, 0xe3, 0x5f, 0x63, 0x5f, 0x82, 0xf0, 0xf7, 0xa1, 0x49, 0x9e, 0x1e, 0x23,
	0xdf, 0x5e, 0xea, 0x1c, 0x91, 0xa2, 0x3c, 0x8c, 0xf7, 0xf7, 0x0d, 0xb8, 0x56, 0x80, 0x5d, 0xee,
	0x87, 0x51, 0x32, 0x66, 0xdf, 0x1a, 0x60, 0x6b, 0xc6, 0xcd, 0x90, 0xcf, 0x26, 0x51, 0xe2, 0xc7,
	0x08, 0x43, 0x17, 0xdf, 0xbe, 0x70, 0x82, 0x55, 0x78, 0xe8, 0xeb, 0xbf, 0xd8, 0x81, 0x75, 0x61,
	0x35, 0x8c, 0x46, 0x23, 0xfc, 0x39, 0x18, 0x1c, 0x3f, 0x41, 0xd7, 0xdb, 0xea, 0xa6, 0x04, 0xe0,
	0x56, 0x19, 0xec, 0x1d, 0xb8, 0x3a, 0x3d, 0xf5, 0x25, 0x1f, 0x0c, 0x86, 0x47, 0xfa, 0xd4, 0x3b,
	0xe5, 0xb2, 0xfb, 0x0a, 0xc9, 0xde, 0x83, 0xe6, 0xac, 0x54, 0x19, 0xba, 0x0b, 0x8f, 0xf5, 0xfb,
	0x0e, 0x5d, 0x97, 0xa2, 0x15, 0xf2, 0xf1, 0x13, 0xeb, 0xca, 0x45, 0x87, 0xa6, 0x68, 0xfb, 0x4f,
	0x13, 0x6e, 0x28, 0x05, 0x21, 0xd3, 0xbc, 0xc4, 0x5d, 0x34, 0xcd, 0xdd, 0x1a, 0xae, 0x9c, 0xe6,
	0x75, 0x85, 0x4b, 0x10, 0xe8, 0x0f, 0xe1, 0x9a, 0xa4, 0x4f, 0x6c, 0xad, 0x6c, 0x37, 0xee, 0x35,
	0x1f, 0xdc, 0xd4, 0x45, 0xba, 0x8c, 0x51, 0xab, 0x94, 0xfb, 0xf1, 0xcc, 0x32, 0x5d, 0x95, 0x8f,
	0xd1, 0xfe, 0xda, 0x84, 0xb5, 0x82, 0x54, 0x0c, 0xb6, 0xd7, 0x72, 0x74, 0xff, 0x6c, 0x40, 0x8b,
	0x9a, 0xc0, 0x1e, 0x42, 0x53, 0xe6, 0x57, 0x74, 0x78, 0xb7, 0x75, 0x6a, 0x74, 0x7a, 0x53, 0x0e,
	0x1b, 0xc2, 0x9a, 0xac, 0xf8, 0x8a, 0x8f, 0x74, 0x47, 0x57, 0xb2, 0x57, 0x28, 0x72, 0xb8, 0x9a,
	0x82, 0xfd, 0x9b, 0x09, 0xd7, 0x29, 0xa1, 0xcc, 0xbd, 0x07, 0xeb, 0xc5, 0xe6, 0xf5, 0xd8, 0x6f,
	0xe9, 0x8e, 0x5d, 0x4f, 0x7d, 0x9d, 0x7f, 0x09, 0x42, 0xff, 0x08, 0x5a, 0xd4, 0x02, 0x1c, 0x31,
	0x9b, 0xcb, 0x0d, 0x24, 0x62, 0x0a, 0x13, 0xd3, 0xfe, 0x97, 0x01, 0xeb, 0x05, 0x1c, 0xdf, 0x5f,
	0x0e, 0xbc, 0x6e, 0xf7, 0xbf, 0xff, 0x48, 0xe0, 0xe5, 0x3b, 0x36, 0xde, 0xf0, 0x82, 0x53, 0x9e,
	0x69, 0x1d, 0x78, 0xdd, 0x47, 0x68, 0xef, 0xdd, 0x4a, 0x95, 0x96, 0xc1, 0xdd, 0xe5, 0x4a, 0xf6,
	0x4b, 0xa3, 0xfc, 0x9b, 0xba, 0x04, 0xdf, 0x15, 0xcf, 0x34, 0x4e, 0x63, 0x1e, 0xb6, 0x74, 0xf5,
	0x23, 0x30, 0x52, 0xc4, 0xba, 0x86, 0xfd, 0xc7, 0x62, 0xf6, 0xbf, 0x36, 0x6f, 0xf2, 0x8f, 0xcb,
	0xd9, 0xaf, 0xbe, 0xc1, 0xdf, 0x3c, 0xc7, 0x47, 0x22, 0xa7, 0x72, 0x8b, 0x4e, 0xd8, 0x7f, 0x61,
	0xc0, 0x0d, 0x31, 0xe5, 0xc9, 0x68, 0x72, 0xa2, 0xa8, 0x38, 0xc6, 0x27, 0xce, 0x38, 0x4a, 0x63,
	0xff, 0x24, 0x7b, 0xd0, 0x0e, 0x42, 0x3a, 0x53, 0x99, 0x5d, 0xcb, 0x4e, 0xfe, 0xcd, 0x7d, 0x32,
	0x1f, 0x75, 0xc6, 0x62, 0x07, 0xd7, 0x76, 0xc4, 0x54, 0xee, 0x2c, 0xee, 0x97, 0x04, 0xaa, 0xf9,
	0xab, 0xa9, 0xdd, 0xea, 0xe4, 0x7f, 0x39, 0x6b, 0xef, 0x9f, 0x00, 0x00, 0x00, 0xff, 0xff, 0x65,
	0x3a, 0x2b, 0x51, 0x1e, 0x10, 0x00, 0x00,
}
