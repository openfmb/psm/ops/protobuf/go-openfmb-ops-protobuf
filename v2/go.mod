module gitlab.com/openfmb/psm/ops/protobuf/go-openfmb-ops-protobuf/v2

go 1.15

require (
	github.com/golang/protobuf v1.4.1
	google.golang.org/protobuf v1.25.0
)
